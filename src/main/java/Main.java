import com.homeworkStream.dataStorage.Cache;
import com.homeworkStream.service.MethodOfInfluencingStudents;

public class Main {
    public static void main(String[] args) {
        MethodOfInfluencingStudents studentMethods = new MethodOfInfluencingStudents();
        Cache cache = new Cache();
        System.out.println(studentMethods.getStudentsGroupList(cache.getListOfStudents(),"group1"));
        System.out.println(studentMethods.getListOfStudentsAboveCertainYear(cache.getListOfStudents(),1992));
    }
}
